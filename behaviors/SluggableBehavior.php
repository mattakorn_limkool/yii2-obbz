<?php
/**
 * @author  Mattakorn Limkool <obbz.dev@gmail.com>
 *
 */

namespace obbz\yii2\behaviors;


class SluggableBehavior extends \yii\behaviors\SluggableBehavior
{
    public $attribute = 'title';
    public $ensureUnique = true;
}