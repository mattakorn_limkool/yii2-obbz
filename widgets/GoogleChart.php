<?php
/**
 * @author: Mattakorn Limkool
 *
 */

namespace obbz\yii2\widgets;


class GoogleChart extends \scotthuangzl\googlechart\GoogleChart
{
    const TYPE_PIE = 'PieChart';
    const TYPE_LINE = 'LineChart';
    const TYPE_COLUMN = 'ColumnChart';
    const TYPE_BAR = 'BarChart';
    const TYPE_AREA = 'AreaChart';
    // have more//
}