<?php
/**
 * @author: Mattakorn Limkool
 *
 */

namespace obbz\yii2\widgets\fileupload;


use yii\web\AssetBundle;

class FileUploadAssets extends AssetBundle
{
    public $sourcePath = '@vendor/obbz/yii2/widgets/fileupload/assets';
    public $css = [
        'style.css'
    ];
}