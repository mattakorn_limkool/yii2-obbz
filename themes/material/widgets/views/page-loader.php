<?php
/**
 * @author: Mattakorn Limkool
 *
 */
?>
<!-- Page Loader -->
<div class="page-loader">
    <div class="preloader pls-<?php echo $skin; ?>">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p>Please wait...</p>
    </div>
</div>
