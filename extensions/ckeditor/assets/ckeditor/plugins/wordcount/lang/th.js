﻿/*
Copyright (c) 2003-2017, CKSource - Mattakorn Limkool. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.plugins.setLang('wordcount', 'th', {
    WordCount: 'จำนวนคำ:',
    WordCountRemaining: 'จำนวนคำที่เหลือ',
    CharCount: 'จำนวนตัวอักษร:',
    CharCountRemaining: 'จำนวนตัวอักษรที่เหลือ',
    CharCountWithHTML: 'จำนวนตัวอักษร และ Html:',
    CharCountWithHTMLRemaining: 'จำนวนตัวอักษร และ Html ที่เหลือ',
    Paragraphs: 'จำนวนบรรทัด:',
    pasteWarning: 'ไม่สามารถวางได้เนื่องจากตัวอักษรมากเกินกำหนด',
    Selected: 'เลือก: ',
    title: ''
});
